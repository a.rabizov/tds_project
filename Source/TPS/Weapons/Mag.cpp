// Fill out your copyright notice in the Description page of Project Settings.


#include "Mag.h"

// Sets default values
AMag::AMag()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MagMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mag Mesh"));
	MagMesh->SetupAttachment(RootComponent);
	MagMesh->SetCanEverAffectNavigation(false);


	MagMesh->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void AMag::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMag::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

