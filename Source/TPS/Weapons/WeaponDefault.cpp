// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include <Kismet/GameplayStatics.h>
#include "Projectiles/BulletShell.h"
#include "Projectiles/BulletShell.h"
#include "TPS/Weapons/Mag.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);
	


}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}


}

void AWeaponDefault::FireTick(float DeltaTime)
{

	if (GetWeaponRound() > 0)
	{
		if (WeaponFiring)
			if (FireTimer < 0.f)
			{
				if (!WeaponReloading)
					Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
	else
	{
		if (!WeaponReloading)
		{
			InitReload();
		}
	}

	
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion -= CurrentDispersionReduction;
			else
				CurrentDispersion += CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: Max = %f. Min = %f, Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round--;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	

	

	int8 NumberProjectiles = GetNumberProjectilesByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FVector EndLocation;


		for (int8 i = 0; i < NumberProjectiles; i++)
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			SpawnBulletShell(SpawnLocation, ShootLocation->GetComponentRotation() - FRotator(0, 90, 0));
			SpawnProjectile(SpawnLocation, SpawnRotation, EndLocation);

		}

		
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		FireTimer = 0.01;
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

void AWeaponDefault::SpawnProjectile(FVector SpawnLocation, FRotator SpawnRotation, FVector EndLocation)
{

	FProjectileInfo ProjectileInfo;
	ProjectileInfo = GetProjectile();
	if (ProjectileInfo.Projectile)
	{

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile,
			&SpawnLocation, &SpawnRotation, SpawnParams));

		if (myProjectile)
		{
			myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
			myProjectile->BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AWeaponDefault::HandleHit);
			
		}

	}
	else
	{
		FHitResult Hit;
		TArray<AActor*> Actors;
		UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace, ETraceTypeQuery::TraceTypeQuery4, false, Actors,
			EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
		
		if (Hit.GetActor())
		{
			HandleHit(Hit.GetComponent(),Hit.GetActor(), Hit.GetComponent(), FVector(0), Hit);
		}
	}
}

void AWeaponDefault::HandleHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
		if (WeaponSetting.HitDecals.Contains(mySurfaceType))
		{
			UMaterialInterface* myMaterial = WeaponSetting.HitDecals[mySurfaceType];
			if (myMaterial && Hit.GetComponent())
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}
		if (WeaponSetting.HitFXs.Contains(mySurfaceType))
		{
			UParticleSystem* myParticle = WeaponSetting.HitFXs[mySurfaceType];
			if (myParticle && Hit.GetComponent())
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		if (WeaponSetting.HitSound.Contains(mySurfaceType))
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.HitSound[mySurfaceType], Hit.ImpactPoint);
		}

		

	}

	UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);

}

void AWeaponDefault::SpawnBulletShell(FVector SpawnLocation, FRotator SpawnRotation)
{
	if (WeaponSetting.BulletShellClass)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();
		FVector BulletShellLocation;

		FVector ForwardImpulseVector;
		FVector RightImpulseVector;
		FVector UpImpulseVector;

		if (SkeletalMeshWeapon)
		{
			BulletShellLocation = SkeletalMeshWeapon->GetSocketLocation("b_gun_shelleject");

			ForwardImpulseVector = SkeletalMeshWeapon->GetForwardVector();
			RightImpulseVector = SkeletalMeshWeapon->GetRightVector();
			UpImpulseVector = SkeletalMeshWeapon->GetUpVector();
		}
		else
		{
			BulletShellLocation = StaticMeshWeapon->GetSocketLocation("b_gun_shelleject");

			ForwardImpulseVector = StaticMeshWeapon->GetForwardVector();
			RightImpulseVector = StaticMeshWeapon->GetRightVector();
			UpImpulseVector = StaticMeshWeapon->GetUpVector();
		}
			
		float RandomPitch = FMath::RandRange(-15.f, 15.f);
		float RandomYaw = FMath::RandRange(-10.f, 10.f);

		FRotator BulletShellRotation = SpawnRotation + FRotator(RandomPitch, RandomYaw, 0.0f);

		ABulletShell* myBulletShell = Cast<ABulletShell>(GetWorld()->SpawnActor(WeaponSetting.BulletShellClass,
			&BulletShellLocation, &BulletShellRotation, SpawnParams));


		if (myBulletShell)
		{
			myBulletShell->InitialLifeSpan = 20.f;

			FVector ImpulseDirection = ForwardImpulseVector * -0.2 + RightImpulseVector * 0.6 + UpImpulseVector * 0.7;
			float ImpulseStrength = 100;
			myBulletShell->BulletShellMesh->AddImpulse(ImpulseDirection * ImpulseStrength);
		}
	}
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimReduction;
		break;

	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionAimReduction;
		break;

	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;

	default:
		break;
		
	}
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer =  WeaponSetting.ReloadTime;
	if(WeaponSetting.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);
	if (WeaponSetting.SoundReloadWeapon)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), WeaponSetting.SoundReloadWeapon);
	}


	//Spawn Magazine
	if (WeaponSetting.MagClass)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();
		FVector MagSpawnLocation;
		FRotator MagSpawnRotation;// = SpawnRotation - FRotator(0, 90, 0);


		if (SkeletalMeshWeapon)
			MagSpawnLocation = SkeletalMeshWeapon->GetSocketLocation("b_gun_mag");
		else
			MagSpawnLocation = StaticMeshWeapon->GetSocketLocation("b_gun_mag");
		AMag* myMag = Cast<AMag>(GetWorld()->SpawnActor(WeaponSetting.MagClass,
			&MagSpawnLocation, &MagSpawnRotation, SpawnParams));
	}


}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	WeaponInfo.Round = WeaponSetting.MaxRound;
	OnWeaponReloadEnd.Broadcast();
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDipersion() * PI / 180.f, GetCurrentDipersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDipersion() * PI / 180.f, GetCurrentDipersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	return EndLocation;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDipersion() * PI / 180.f);
}

float AWeaponDefault::GetCurrentDipersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

int8 AWeaponDefault::GetNumberProjectilesByShot() const
{
	return WeaponSetting.NumberProjectilesByShot;
}



