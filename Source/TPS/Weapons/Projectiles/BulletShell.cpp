// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletShell.h"

// Sets default values
ABulletShell::ABulletShell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletShellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Shell Mesh"));
	BulletShellMesh->SetupAttachment(RootComponent);
	BulletShellMesh->SetCanEverAffectNavigation(false);


	BulletShellMesh->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void ABulletShell::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABulletShell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

